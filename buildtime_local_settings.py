# SPDX-License-Identifier: Apache-2.0


# This is a minimal local_settings.conf used during build-time
#  to keep static resources where runtime Horizon expects them

import os

DEBUG = False
TEMPLATE_DEBUG = False

COMPRESS_OFFLINE = True
COMPRESS_ENABLED = True

LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = "/srv/app/static"

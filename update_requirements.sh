#!/usr/bin/env bash
# Build wheels for distribution
set -o errexit
set -o nounset
set -o pipefail

BASE=$(realpath $(dirname $0))
BUILD=${BASE}/_build
VENV=${BUILD}/venv
HORIZON=${BASE}/horizon
KEYSTONECLIENT=${BASE}/keystoneclient
DESIGNATEDASH=${BASE}/designate-dashboard
TROVEDASH=${BASE}/trove-dashboard
BARBICANDASH=${BASE}/barbican-ui
SUDODASH=${BASE}/wmf-sudo-dashboard
PUPPETDASH=${BASE}/wmf-puppet-dashboard
WHEEL_DIR=${BASE}/wheels
REQUIREMENTS=${BASE}/requirements.txt

PIP=${VENV}/bin/pip

mkdir -p $VENV
virtualenv --python python3 $VENV || /bin/true
$PIP install --upgrade pip setuptools==45
$PIP install -c ${BASE}/upper-constraints.txt -r ${HORIZON}/requirements.txt -r ${KEYSTONECLIENT}/requirements.txt -r ${DESIGNATEDASH}/requirements.txt -r ${TROVEDASH}/requirements.txt -r ${BARBICANDASH}/requirements.txt -r ${SUDODASH}/requirements.txt -r ${PUPPETDASH}/requirements.txt
$PIP freeze --local --requirement $REQUIREMENTS > $REQUIREMENTS

# This is just a bug as far as I can tell.
sed -i '/pkg-resources==0.0.0/d' ./requirements.txt

# We're going to install Horizon froms ource so don't want this in there
sed -i '/^horizon==.*$/d' ./requirements.txt

# We need wheels for pip and setuptools for creating the venv on the target
echo "pip>=9.0" >> ./requirements.txt
echo "setuptools>=38.0" >> ./requirements.txt

#!/usr/bin/env bash
# Install submodule-hosted components into PYPATH and compress static content
set -o errexit
set -o nounset
set -o pipefail
set -x

DEPLOY_DIR=/srv/app
PYPATH=/opt/lib/python/site-packages
POLICYDIR=${DEPLOY_DIR}/policy
CONSTRAINTS=${DEPLOY_DIR}/requirements/upper-constraints.txt

# https://docs.openstack.org/pbr/latest/user/packagers.html
export SKIP_GIT_SDIST=1
export SKIP_GENERATE_AUTHORS=1
export SKIP_WRITE_GIT_CHANGELOG=1
export SKIP_GENERATE_RENO=1

export PIP_NO_CACHE_DIR=1
export PIP_DISABLE_PIP_VERSION_CHECK=1

function find_version {
    local package=${1:?package name required}
    local pin=$(grep "${package}" ${CONSTRAINTS})
    echo ${pin##*=}
}
# Set a default version string for pbr built packages.
# This must be overridden for packages that are listed in $CONSTRAINTS
export PBR_VERSION=$(date '+%Y.%m.%d')

mkdir ${POLICYDIR}
mkdir ${POLICYDIR}/default_policies

# submodule update
cd $DEPLOY_DIR/horizon && git submodule update --init --recursive --depth 1

function pip_install {
    python3 -m pip install -c ${CONSTRAINTS} --target=${PYPATH} "$@"
}

function enable_dashboard {
    local dashboard=${1:?dashboard directory required}
    cp -r ${dashboard}/enabled/* ${PYPATH}/openstack_dashboard/local/enabled/
}

function install_policies {
    local dashboard=${1:?dashboard directory required}
    cp ${dashboard}/conf/default_policies/* ${POLICYDIR}/default_policies/
}

function run_manage {
    cd ${PYPATH} && python3 ${PYPATH}/horizon/manage.py "$@"
}

# install Horizon
PBR_VERSION=$(find_version horizon) pip_install $DEPLOY_DIR/horizon
# When we collect static resources, later, we'll want manage.py to be in the
#  PYPATH because it does some dumb relative-path things.  I'm not clear on
#  why Horizon doesn't install this itself.
cp $DEPLOY_DIR/horizon/manage.py  ${PYPATH}/horizon/
install_policies $DEPLOY_DIR/horizon/openstack_dashboard

# Remove whatever version of keystoneclient was installed by pip
#  because we definitely want our own install from source.
rm -rf ${PYPATH}/keystoneclient/
PBR_VERSION=$(find_version python-keystoneclient) pip_install $DEPLOY_DIR/keystoneclient

# install designate-dashboard
pip_install $DEPLOY_DIR/designate-dashboard
enable_dashboard ${DEPLOY_DIR}/designate-dashboard/designatedashboard
#install_policies $DEPLOY_DIR/designate-dashboard/designatedashboard

# install trove-dashboard
pip_install $DEPLOY_DIR/trove-dashboard
enable_dashboard ${DEPLOY_DIR}/trove-dashboard/trove_dashboard
#install_policies $DEPLOY_DIR/trove-dashboard/trove_dashboard

# install magnum-ui
pip_install $DEPLOY_DIR/magnum-ui
enable_dashboard ${DEPLOY_DIR}/magnum-ui/magnum_ui
#install_policies $DEPLOY_DIR/magnum-ui/magnum_ui

# install barbican-ui
# Marked out pending future adoption
# pip_install $DEPLOY_DIR/barbican-ui
# install_policies ${DEPLOY_DIR}/barbican-ui/barbican_ui

# install sudo-dashboard
pip_install $DEPLOY_DIR/wmf-sudo-dashboard
enable_dashboard ${DEPLOY_DIR}/wmf-sudo-dashboard

# install sudo-member-dashboard
pip_install $DEPLOY_DIR/wmf-member-dashboard
enable_dashboard ${DEPLOY_DIR}/wmf-member-dashboard

# install proxy-dashboard
pip_install $DEPLOY_DIR/wmf-proxy-dashboard
enable_dashboard ${DEPLOY_DIR}/wmf-proxy-dashboard

# install puppet-dashboard
pip_install $DEPLOY_DIR/wmf-puppet-dashboard
enable_dashboard ${DEPLOY_DIR}/wmf-puppet-dashboard

# Horizon looks in all kinds of places for local_settings.py, let's keep things simple
rm -f ${DEPLOY_DIR}/horizon/openstack_dashboard/local/local_settings.py

# Install minimal config to configure the next few steps. This will be replaced
#  by a mount at runtime.
cp ${DEPLOY_DIR}/buildtime_local_settings.py ${PYPATH}/openstack_dashboard/local/local_settings.py

# Update localization files
run_manage compilemessages

# Gather up and compress static content
run_manage collectstatic -c --noinput
run_manage collectstatic --noinput
run_manage compress --force

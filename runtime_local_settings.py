# SPDX-License-Identifier: Apache-2.0
#####################################################################
### THIS FILE IS MANAGED BY PUPPET
#####################################################################
import os

from django.utils.translation import gettext_lazy as _

from horizon.utils import secret_key

from openstack_dashboard.settings import HORIZON_CONFIG

from openstack_dashboard import exceptions

from django.utils.translation import gettext_lazy as _

DEBUG = True
TEMPLATE_DEBUG = DEBUG


SECRET_KEY = "localdevkey"

# Required for Django 1.5.
# If horizon is running in production (DEBUG is False), set this
# with the list of host/domain names that the application can serve.
# For more information see:
# https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
# ALLOWED_HOSTS = ['horizon.wikimedia.org', ]

AUTHENTICATION_PLUGINS = ['openstack_auth.plugin.wmtotp.WmtotpPlugin', 'openstack_auth.plugin.token.TokenPlugin']

# SESSION_TIMEOUT is in seconds and defaults to 1800.  Change to 7 days
#  by default, and also support SESSION_SHORT_TIMEOUT of 30 minutes.
#
# SESSION_SHORT is a WMF hack.  SESSION_TIMEOUT is used in multiple
#  places as a maximum in Horizon code, so it was easier to add
#  a special case extra-short token for when the user does not
#  check 'Remember me'.
SESSION_SHORT_TIMEOUT = 1800
SESSION_TIMEOUT = 604800
SESSION_COOKIE_AGE = 604800

CSRF_COOKIE_SECURE = False
CSRF_COOKIE_HTTPONLY = False

SESSION_COOKIE_SECURE = False
SESSION_COOKIE_HTTPONLY = False


# Overrides for OpenStack API versions. Use this setting to force the
# OpenStack dashboard to use a specific API version for a given service API.
# NOTE: The version should be formatted as it appears in the URL for the
# service API. For example, The identity service APIs have inconsistent
# use of the decimal point, so valid options would be "2.0" or "3".
# OPENSTACK_API_VERSIONS = {
#     "identity": 3,
#     "volume": 2
# }

OPENSTACK_API_VERSIONS = {
     "identity": 3,
     "image": 2,
}

# Set this to True if running on multi-domain model. When this is enabled, it
# will require user to enter the Domain name in addition to username for login.
# OPENSTACK_KEYSTONE_MULTIDOMAIN_SUPPORT = False

# Overrides the default domain used when running on single-domain model
# with Keystone V3. All entities will be created in the default domain.
OPENSTACK_KEYSTONE_DEFAULT_DOMAIN = 'default'

# Set Console type:
# valid options would be "AUTO", "VNC", "SPICE" or "RDP"
# CONSOLE_TYPE = "AUTO"

HORIZON_CONFIG['help_url'] = "https://wikitech.wikimedia.org/wiki/Horizon"

# Disable simplified floating IP address management for deployments with
# multiple floating IP pools or complex network requirements.
# HORIZON_CONFIG["simple_ip_management"] = False

# Turn on browser autocompletion for the login form
HORIZON_CONFIG["password_autocomplete"] = "on"

LOCAL_PATH = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = "/srv/app/static"

# Enable the Ubuntu theme if it is present.
try:
	from ubuntu_theme import *
except ImportError:
	pass

# Default Ubuntu apache configuration uses /horizon as the application root.
# Configure auth redirects here accordingly.
LOGIN_URL='/auth/login/'
LOGOUT_URL='/auth/logout/'
LOGIN_REDIRECT_URL='/'

COMPRESS_OFFLINE = True
COMPRESS_ENABLED = True

# By default, validation of the HTTP Host header is disabled.  Production
# installations should have this set accordingly.  For more information
# see https://docs.djangoproject.com/en/dev/ref/settings/.
ALLOWED_HOSTS = ['*']


OPENSTACK_HOST = "openstack.eqiad1.wikimediacloud.org"
OPENSTACK_KEYSTONE_URL = "https://%s:25000/v3" % OPENSTACK_HOST
OPENSTACK_KEYSTONE_DEFAULT_ROLE = "reader"
DEFAULT_SERVICE_REGIONS = {
    OPENSTACK_KEYSTONE_URL: 'eqiad'
}

OPENSTACK_SSL_NO_VERIFY = True

# The OPENSTACK_KEYSTONE_BACKEND settings can be used to identify the
# capabilities of the auth backend for Keystone.
# If Keystone has been configured to use LDAP as the auth backend then set
# can_edit_user to False and name to 'ldap'.
#
# TODO(tres): Remove these once Keystone has an API to identify auth backend.
OPENSTACK_KEYSTONE_BACKEND = {
    'name': 'ldap',
    'can_edit_user': False,
    'can_edit_group': False,
    'can_edit_project': True,
    'can_edit_domain': True,
    'can_edit_role': True
}

# The OPENSTACK_NEUTRON_NETWORK settings can be used to enable optional
# services provided by neutron. Options currently available are load
# balancer service, security groups, quotas, VPN service.
OPENSTACK_NEUTRON_NETWORK = {
    'enable_lb': False,
    'enable_firewall': False,
    'enable_quotas': True,
    'enable_vpn': False,
    # The profile_support option is used to detect if an external router can be
    # configured via the dashboard. When using specific plugins the
    # profile_support can be turned on if needed.
    'profile_support': None,
    #'profile_support': 'cisco',
}

# The OPENSTACK_IMAGE_BACKEND settings can be used to customize features
# in the OpenStack Dashboard related to the Image service, such as the list
# of supported image formats.
OPENSTACK_IMAGE_BACKEND = {
    'image_formats': [
        ('iso', _('ISO - Optical Disk Image')),
        ('qcow2', _('QCOW2 - QEMU Emulator')),
        ('raw', _('Raw')),
        ('vdi', _('VDI')),
        ('vhd', _('VHD')),
        ('vmdk', _('VMDK'))
    ]
}

# The IMAGE_CUSTOM_PROPERTY_TITLES settings is used to customize the titles for
# image custom property attributes that appear on image detail pages.
IMAGE_CUSTOM_PROPERTY_TITLES = {
    "architecture": _("Architecture"),
    "kernel_id": _("Kernel ID"),
    "ramdisk_id": _("Ramdisk ID"),
    "image_state": _("Euca2ools state"),
    "project_id": _("Project ID"),
    "image_type": _("Image Type")
}

# The number of objects (Swift containers/objects or images) to display
# on a single page before providing a paging element (a "more" link)
# to paginate results.
API_RESULT_LIMIT = 1000
API_RESULT_PAGE_SIZE = 20

# The timezone of the server. This should correspond with the timezone
# of your entire OpenStack installation, and hopefully be in UTC.
TIME_ZONE = "UTC"


# The Horizon Policy Enforcement engine uses these values to load per service
# policy rule files. The content of these files should match the files the
# OpenStack services are using to determine role based access control in the
# target installation.

# Path to directory containing policy.json files
POLICY_FILES_PATH = '/etc/openstack-dashboard/'
# Map of local copy of service policy files
POLICY_FILES = {
    'identity': 'keystone_policy.yaml',
    'compute': 'nova_policy.yaml',
    'image': 'glance_policy.yaml',
    'dns': 'designate_policy.yaml',
    'network': 'neutron_policy.yaml',
    'volume': 'cinder_policy.yaml',
    'database': 'trove_policy.yaml',
    'telemetry': 'disabled_policy.yaml',
    'orchestration': 'disabled_policy.yaml',
}

# Trove user and database extension support. By default support for
# creating users and databases on database instances is turned on.
# To disable these extensions set the permission here to something
# unusable such as ["!"].
# TROVE_ADD_USER_PERMS = []
# TROVE_ADD_DATABASE_PERMS = []

LOGGING = {
    'version': 1,
    # When set to True this will disable all logging except
    # for loggers specified in this configuration dictionary. Note that
    # if nothing is specified here and disable_existing_loggers is True,
    # django.db.backends will still log unless it is disabled explicitly.
    'disable_existing_loggers': False,
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'console': {
            # Set the level to "DEBUG" for verbose output logging.
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        # Logging from django.db.backends is VERY verbose, send to null
        # by default.
        'django.db.backends': {
            'handlers': ['console'],
            'propagate': True,
        },
        'requests': {
            'handlers': ['console'],
            'propagate': False,
        },
        'horizon': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'openstack_dashboard': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'novaclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'cinderclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'keystoneclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'glanceclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'neutronclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'heatclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'ceilometerclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'troveclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'swiftclient': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'openstack_auth': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'nose.plugins.manager': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'django': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        },
        'iso8601': {
            'handlers': ['console'],
            'propagate': False,
        },
    }
}

# 'direction' should not be specified for all_tcp/udp/icmp.
# It is specified in the form.
SECURITY_GROUP_RULES = {
    'all_tcp': {
        'name': 'ALL TCP',
        'ip_protocol': 'tcp',
        'from_port': '1',
        'to_port': '65535',
    },
    'all_udp': {
        'name': 'ALL UDP',
        'ip_protocol': 'udp',
        'from_port': '1',
        'to_port': '65535',
    },
    'all_icmp': {
        'name': 'ALL ICMP',
        'ip_protocol': 'icmp',
        'from_port': '-1',
        'to_port': '-1',
    },
    'ssh': {
        'name': 'SSH',
        'ip_protocol': 'tcp',
        'from_port': '22',
        'to_port': '22',
    },
    'smtp': {
        'name': 'SMTP',
        'ip_protocol': 'tcp',
        'from_port': '25',
        'to_port': '25',
    },
    'dns': {
        'name': 'DNS',
        'ip_protocol': 'tcp',
        'from_port': '53',
        'to_port': '53',
    },
    'http': {
        'name': 'HTTP',
        'ip_protocol': 'tcp',
        'from_port': '80',
        'to_port': '80',
    },
    'pop3': {
        'name': 'POP3',
        'ip_protocol': 'tcp',
        'from_port': '110',
        'to_port': '110',
    },
    'imap': {
        'name': 'IMAP',
        'ip_protocol': 'tcp',
        'from_port': '143',
        'to_port': '143',
    },
    'ldap': {
        'name': 'LDAP',
        'ip_protocol': 'tcp',
        'from_port': '389',
        'to_port': '389',
    },
    'https': {
        'name': 'HTTPS',
        'ip_protocol': 'tcp',
        'from_port': '443',
        'to_port': '443',
    },
    'smtps': {
        'name': 'SMTPS',
        'ip_protocol': 'tcp',
        'from_port': '465',
        'to_port': '465',
    },
    'imaps': {
        'name': 'IMAPS',
        'ip_protocol': 'tcp',
        'from_port': '993',
        'to_port': '993',
    },
    'pop3s': {
        'name': 'POP3S',
        'ip_protocol': 'tcp',
        'from_port': '995',
        'to_port': '995',
    },
    'ms_sql': {
        'name': 'MS SQL',
        'ip_protocol': 'tcp',
        'from_port': '1433',
        'to_port': '1433',
    },
    'mysql': {
        'name': 'MYSQL',
        'ip_protocol': 'tcp',
        'from_port': '3306',
        'to_port': '3306',
    },
    'rdp': {
        'name': 'RDP',
        'ip_protocol': 'tcp',
        'from_port': '3389',
        'to_port': '3389',
    },
}

FLAVOR_EXTRA_KEYS = {
    'flavor_keys': [
        ('quota:read_bytes_sec', _('Quota: Read bytes')),
        ('quota:write_bytes_sec', _('Quota: Write bytes')),
        ('quota:cpu_quota', _('Quota: CPU')),
        ('quota:cpu_period', _('Quota: CPU period')),
        ('quota:inbound_average', _('Quota: Inbound average')),
        ('quota:outbound_average', _('Quota: Outbound average')),
    ]
}

# WMF-specific branding
SITE_BRANDING = 'Manage Wikimedia Cloud'
SITE_BRANDING_LINK = 'https://horizon.wikimedia.org'

# WMF-specific security limits
HORIZON_IMAGES_UPLOAD_MODE = 'off'

DESIGNATE = { 'records_use_fips': True }

INSTANCE_TLD = 'eqiad1.wikimedia.cloud'

# Disable instance consoles for Horizon users
CONSOLE_TYPE = False

GITTILES_BASE_URL = "https://gerrit.wikimedia.org/r/plugins/gitiles/cloud/instance-puppet/+/master/"

# The following sections won't work at all with local test/dev. This means
#  various wikimedia-specific panels won't work locally.
LDAP_USER = "uid=novaadmin,ou=people,dc=wikimedia,dc=org"
ADMIN_USER = "novaadmin"
ADMIN_PROJECT = "admin"
LDAP_PROJECTS_BASE = "ou=projects,dc=wikimedia,dc=org"
LDAP_RW_URI = "ldap://ldap-rw.eqiad.wikimedia.org"
LDAP_USER_PASSWORD = "fakepass"


# A few things for role management:
MEMBER_ROLE_NAME = "reader"
PROJECTADMIN_ROLE_NAME = "member"

# 'Special' users are excluded from normal UI interfaces.
#  For example, we don't want to set up sudo policies for 'novaobserver'.
SPECIAL_USERS = ['novaadmin', 'novaobserver']

# We don't display the network-selection dialog during VM creation;
#  instead just use this network.
LAUNCH_INSTANCE_DEFAULTS = {"network_id": "7425e328-560c-4f00-8e99-706f3fb90bb4",
                            "hide_create_volume": True,
                            "create_volume": False,
                            "disable_instance_snapshot": True,
                            "disable_volume": True,
                            "disable_volume_snapshot": True}

# This is documented as the default, but I've found it to be unset in
#  testing.  It's needed for the above setting to get picked up
#  by horizon JS code.
REST_API_REQUIRED_SETTINGS = [
    'OPENSTACK_HYPERVISOR_FEATURES',
    'LAUNCH_INSTANCE_DEFAULTS',
    'OPENSTACK_IMAGE_FORMATS',
    'OPENSTACK_KEYSTONE_BACKEND',
    'OPENSTACK_KEYSTONE_DEFAULT_DOMAIN',
    'CREATE_IMAGE_DEFAULTS',
    'ENFORCE_PASSWORD_CHECK'
]

# Just show us all the projects. A scroll bar is better than
#  mysterious truncation
DROPDOWN_MAX_ITEMS=1000000

#SESSION_ENGINE = 'django.contrib.sessions.backends.cache'
SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'
CACHES = {
  'default' : {
    'BACKEND': 'django.core.cache.backends.locmem.LocMemCache'
  }
}

# Just show us all the projects. A scroll bar is better than
#  mysterious truncation
DROPDOWN_MAX_ITEMS=1000000

# Enable SSO things
WEBSSO_ENABLED=True
WEBSSO_CHOICES = (
    ("credentials", _("Keystone Credentials")),
    ("openid", _("OpenID Connect")),
)
WEBSSO_INITIAL_CHOICE = "openid"

# Comment out the next two lines to skip sso and authenticate
#  via keystone directly
WEBSSO_DEFAULT_REDIRECT = True
WEBSSO_DEFAULT_REDIRECT_PROTOCOL = "openid"
WEBSSO_DEFAULT_REDIRECT_REGION = OPENSTACK_KEYSTONE_URL

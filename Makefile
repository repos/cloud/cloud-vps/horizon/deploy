this := $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST))
PROJECT_DIR := $(dir $(this))
PIPELINE_DIR := $(PROJECT_DIR)/.pipeline

help:
	@echo "Make targets:"
	@echo "============="
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
		awk 'BEGIN {FS = ":.*?## "}; {printf "%-20s %s\n", $$1, $$2}'
.PHONY: help

start:  ## Start the docker compose stack
	docker compose up --build --detach
.PHONY: start

stop:  ## Stop the docker compose stack
	docker compose stop
.PHONY: stop

restart: stop start  ## Restart the docker compose stack
.PHONY: restart

status:  ## Show status of the docker compose stack
	docker compose ps
.PHONY: status

tail:  ## Tail logs from the docker compose stack
	docker compose logs --tail=1000 -f
.PHONY: tail

shell:  ## Open a shell in the horizon container
	docker compose exec horizon /bin/bash
.PHONY: shell
